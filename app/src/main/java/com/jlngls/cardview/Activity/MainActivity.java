package com.jlngls.cardview.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.jlngls.cardview.Adapter.Adapter;
import com.jlngls.cardview.Adapter.Adapter2;
import com.jlngls.cardview.Model.Model;
import com.jlngls.cardview.Model.Model2;
import com.jlngls.cardview.R;
import com.jlngls.cardview.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView recyclerView2;
    private List<Model> postagens = new ArrayList<>();
    private List<Model2> postagens2 = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerPostagemID);
        recyclerView2 = findViewById(R.id.recyclerView2ID);


        // Card View 1
        this.criarPostagem();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        RecyclerView.Adapter adapter = new Adapter(this,postagens);
        recyclerView.setAdapter(adapter);

        // Card View 2

        this.criarPostagem2();
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(layoutManager1);
        recyclerView2.setHasFixedSize(true);

        RecyclerView.Adapter adapter1 = new Adapter2(this,postagens2);
        recyclerView2.setAdapter(adapter1);


    }

    private void criarPostagem() {
        Model postar = new Model("John Lennon", R.drawable.imagem1, "#Tbt Viagem legal!!");
        postagens.add(postar);

        postar = new Model("Suzanne Lobo", R.drawable.imagem2, "Bom dia!!");
        postagens.add(postar);

        postar = new Model("Luciene Lobo", R.drawable.imagem3, "Deus é fiel");
        postagens.add(postar);

        postar = new Model("Carmocir Araujo", R.drawable.imagem4, "Viva a natureza");
        postagens.add(postar);
    }

    private void criarPostagem2() {
        Model2 postar = new Model2("Antonio Schneider", R.drawable.imagem5, "#Tbt Viagem legal!!");
        postagens2.add(postar);

        postar = new Model2("Igor Santos", R.drawable.imagem6, "Bom dia!!");
        postagens2.add(postar);

        postar = new Model2("Jennyfer Lobo", R.drawable.imagem7, "Deus é fiel");
        postagens2.add(postar);

        postar = new Model2("Moacir Vieira", R.drawable.imagem8, "Viva a natureza");
        postagens2.add(postar);

        postar = new Model2("Milton Cortez", R.drawable.imagem9, "Viva a natureza");
        postagens2.add(postar);

        postar = new Model2("Marcus Vinicius", R.drawable.imagem10, "Viva a natureza");
        postagens2.add(postar);
    }


}
package com.jlngls.cardview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.cardview.Activity.MainActivity;
import com.jlngls.cardview.Model.Model;
import com.jlngls.cardview.MyViewHolder.MyViewHolder;
import com.jlngls.cardview.R;

import java.util.List;
import java.util.zip.Inflater;


public class Adapter extends RecyclerView.Adapter<MyViewHolder> {

    private List<Model> postagens;
    private Context cx;

    public Adapter(Context context,List<Model> listapostagens) {
        postagens = listapostagens;
        cx = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View lista = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cards,parent,false);

        return new MyViewHolder(lista);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        Model postagem = postagens.get(position);
        holder.titulo.setText(postagem.titulo);
        holder.legenda.setText(postagem.legenda);
        holder.imagem.setImageResource(postagem.imagem);
        holder.buttonCurtir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    holder.buttonCurtir.setBackgroundDrawable(cx.getResources().getDrawable(R.drawable.button_costumize));
                }else if(isChecked){
                    holder.buttonCurtir.setBackgroundDrawable(cx.getResources().getDrawable(R.drawable.button_curtir_onclick));

                }
            }
        });


    }




    @Override
    public int getItemCount() {
        return postagens.size();
    }

}

package com.jlngls.cardview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.cardview.Model.Model;
import com.jlngls.cardview.Model.Model2;
import com.jlngls.cardview.MyViewHolder.MyViewHolder;
import com.jlngls.cardview.R;

import java.util.List;

public class Adapter2 extends RecyclerView.Adapter<Adapter2.MyViewHolder2>{

        private List<Model2> postagens2;
        private Context cxt;

        public Adapter2(  Context context ,List<Model2> listapostagens2) {
            postagens2 = listapostagens2;
            cxt = context;

        }

        @NonNull
        @Override
        public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View lista2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cards2,parent,false);

            return new MyViewHolder2(lista2);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder2 holder, int position) {

            final Model2 postagem2 = postagens2.get(position);
            holder.titulo2.setText(postagem2.titulo2);
            holder.legenda2.setText(postagem2.legenda2);
            holder.imagem2.setImageResource(postagem2.imagem2);
            holder.botaoCurtir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if(!isChecked){
                    holder.botaoCurtir.setBackgroundDrawable(cxt.getResources().getDrawable(R.drawable.button_costumize));

                    }else  if(isChecked){
                    holder.botaoCurtir.setBackgroundDrawable(cxt.getResources().getDrawable(R.drawable.button_curtir_onclick));
                        Toast.makeText(cxt, "Voce curtiu a publicação de " + postagem2.titulo2, Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return postagens2.size();
        }



        public class MyViewHolder2 extends RecyclerView.ViewHolder{

            private TextView titulo2;
            private TextView legenda2;
            private ImageView imagem2;
            private ToggleButton botaoCurtir;
            private Button botaoComentar;

            public MyViewHolder2(@NonNull View itemView) {
                super(itemView);

                titulo2 = itemView.findViewById(R.id.tituloID);
                legenda2 = itemView.findViewById(R.id.legendaID2);
                imagem2 = itemView.findViewById(R.id.imagemID2);
                botaoCurtir = itemView.findViewById(R.id.buttonCurtirID2);
                botaoComentar = itemView.findViewById(R.id.buttonComentarID2);

            }
        }
    }



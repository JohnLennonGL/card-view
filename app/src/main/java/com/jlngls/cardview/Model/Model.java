package com.jlngls.cardview.Model;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Model {
    public String titulo;
    public int imagem;
    public String legenda;


    public Model(String titulo, int imagem, String legenda) {
        this.titulo = titulo;
        this.imagem = imagem;
        this.legenda = legenda;

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public String getLegenda() {
        return legenda;
    }

    public void setLegenda(String legenda) {
        this.legenda = legenda;
    }

}


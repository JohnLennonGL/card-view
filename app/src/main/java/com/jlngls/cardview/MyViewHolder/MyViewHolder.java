package com.jlngls.cardview.MyViewHolder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.cardview.R;

public class MyViewHolder extends RecyclerView.ViewHolder {

public TextView titulo;
public ImageView imagem;
    public TextView legenda;
    public Button buttonComentar;
    public ToggleButton buttonCurtir;


    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        titulo = itemView.findViewById(R.id.tituloID);
        imagem = itemView.findViewById(R.id.imagemID);
        legenda = itemView.findViewById(R.id.legendaID);
        buttonComentar = itemView.findViewById(R.id.buttonComentarID);
        buttonCurtir = itemView.findViewById(R.id.buttonCurtirID);



    }
}
